// Declaración de arreglos
var arreglo = [19, 20, 3, 2, 6, 3, 3, 9, 50];

//Mostrar los elementos del arreglo

function mostrarArray() {
    for (con = 0; con < arreglo.length; con++) {
        console.log(con + ":" + arreglo[con]);
    }
}

//Funcion que retorna el promedio de los elemntos del arreglo
function promedio() {
    let prom = 0;
    for (con = 0; con < arreglo.length; con++) {
        prom += arreglo[con];
    }

    return prom / arreglo.length;

}

//Realizar una funcion que regrese  un valor entero que represente la cantidad de numeros  pares en el arreglo
function pares() {
    let par = 0;
    for (con = 0; con < arreglo.length; con++) {
        if (arreglo[con] % 2 == 0) {
            par += 1;
        }
    }
    return par;
}

//realizar una funcion que regrese el numero mayor contenido en el array
//Se modifico para la pagina
function mayor(array) {
    let may = 0;
    for (con = 0; con < array.length; con++) {
        if (array[con] > may) {
            may = array[con];
        }
    }
    return may;
}

//Funcion para encontrar el numero menor
function menor(array) {
    let men = array[0];
    for (let con = 0; con < array.length; con++) {
        if (array[con] < men) {
            men = array[con];
        }
    }
    return men;
}

//Funcion que muestra si el porcentaje de los elementos del arreglo es siemtrico o no
function porcentajeSimetria(array) {
    // Contar el número de elementos pares e impares en el arreglo
    let pares = 0;
    let impares = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            pares++;
        } else {
            impares++;
        }
    }
    // Calcular el porcentaje de simetría
    let porcentajeSimetria = (pares / array.length) * 100;
    // Devolver el porcentaje de simetría
    return porcentajeSimetria;
}

//Funcion para sacar el porcentaje de numeros pares del arreglo
function porcentajePares(array) {
    let contador = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 === 0) {
            contador++
        }
    }
    let porcentajePare = ((contador / array.length) * 100).toFixed(2);
    return porcentajePare;
}
//Funcion para sacar el porcentaje de numeros impares del arreglo
function porcentajeImpares(array) {
    let contador = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] % 2 !== 0) {
            contador++
        }
    }
    let porcentajeImpare = ((contador / array.length) * 100).toFixed(2);
    return porcentajeImpare;
}

/*Diseñe una funcion que recibe un valor numerico que indica la cantidad de elementos del arreglo
  El arreglo debera llenarse con valores aleatorios en el rango del 0 al 1000 posteriormente
  mostrara el arreglo*/

function nums(rango) {
    const cmbAleatorio = document.getElementById('cmbNumeros');
    let arr = [];
    for (cont = 0; cont < rango; cont++) {
        arr[cont] = Math.floor(Math.random() * 1000);

        let option = document.createElement('option');
        option.value = arr[cont];
        option.innerHTML = arr[cont];
        cmbAleatorio.appendChild(option);
    }
    return arr;
}

//Funcion para sacar el promedio de los numeros del arreglo
function promNumero(tNums) {
    let suma = 0;
    for (let i = 0; i < tNums.length; i++) {
        suma += parseInt(tNums[i]);
    }
    let prom = suma / tNums.length;
    return prom;
}

//Funcion para encontrar el numero mayor del arreglo y mostrar en que indice se encuenra
function numMay(tNums) {
    may = mayor(tNums);
    posicion = tNums.indexOf(may);
    document.getElementById('mayor').innerHTML = "Mayor: " + may + " Indice: " + posicion;
}

function numMen(tNums) {
    men = menor(tNums);
    posicion = tNums.indexOf(men);
    document.getElementById('menor').innerHTML = "Menor: " + men + " Indice: " + posicion;
}

function porce(tNums) {
    porce = porcentajeSimetria(tNums);
    document.getElementById('simetrco').innerHTML = "Simetrico: " + porce.toFixed(2) + "%";
}

function porce_May_Men(tNums){
    document.getElementById('porPares').innerHTML = "Porcentaje de pares: "+ porcentajePares(tNums) + "%";
    document.getElementById('porImp').innerHTML = "Porcentaje de impares: "+ porcentajeImpares(tNums) + "%";
}



//Declaracion de la funcion para mostrar los numeros random
function mostrar() {
    // Obtener el elemento select
    const selectElement = document.getElementById('cmbNumeros');
    // Eliminar todas las opciones del select
    while (cmbNumeros.firstChild) {
        cmbNumeros.removeChild(cmbNumeros.firstChild);
    }

    //Hacer el llenado y mostrar los resultados
    let rango = document.getElementById('range').value;
    let tNums = nums(rango);

    document.getElementById('promedio').innerHTML = "Promedio:" + promNumero(tNums).toFixed(2);
    //Esta funcion modifica directamente la etiqueta ya que requiere 2 valores para mostrar
    numMay(tNums);
    numMen(tNums);
    porce(tNums);
    porce_May_Men(tNums);
}

//Llamadas a las funciones 
//mostrarArray();

var promedio = promedio();
console.log("El promedio de los elementos del arreglo es: " + promedio.toFixed(2));

var numPar = pares();
console.log("El arreglo contiene " + numPar + " Numeros pares");

var numMay = mayor();
console.log("El numero mayor es: " + numMay);

console.log("Numeros random");