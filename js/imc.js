const btnCalcular = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function(){
    let edad = parseInt(document.getElementById('txtEdad'.value));
    let sexo = document.querySelector('input[type="radio"][name="sexo"]:checked').value;
    let peso = parseFloat(document.getElementById('txtPeso').value);
    let altura = parseFloat(document.getElementById('txtAltura').value);
    let img = document.getElementById('imgC');

    //Hacer el calculo del imc y mostrarlo
    let imc = peso / (altura * altura);
    document.getElementById('txtResult').value = imc.toFixed(1);
    switch(sexo){
        //Caso de imagenes de seleccionar hombre
        case 'Hombre': {
            if(imc < 18.5){
                img.src = "../img/01H.png";
            }
            else if(imc >= 18.5 && imc <= 24.9){
                img.src = "../img/02H.png";
            }
            else if(imc >= 25 && imc <= 29){
                img.src = "../img/03H.png";
            }
            else if(imc >= 30 && imc <= 34.9){
                img.src = "../img/04H.png";
            }
            else if(imc >= 35 && imc <= 39.9){
                img.src= "../img/05H.png";
            }
            else if(imc > 40){
                img.src="../img/06H.png";
            }
            break;
        }

        case 'Mujer': {
            if(imc < 18.5){
                img.src = "../img/01M.png";
            }
            else if(imc >= 18.5 && imc <= 24.9){
                img.src = "../img/02M.png";
            }
            else if(imc >= 25 && imc <= 29){
                img.src = "../img/03M.png";
            }
            else if(imc >= 30 && imc <= 34.9){
                img.src = "../img/04M.png";
            }
            else if(imc >= 35 && imc <= 39.9){
                img.src= "../img/05M.png";
            }
            else if(imc > 40){
                img.src="../img/06M.png";
            }
            break;
        }
    }
});